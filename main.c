#include "stdio.h"
#include "string.h"
#include "stdlib.h"

#define USUARIO "admin"
#define CLAVE "clave"
#define LONGITUDLOGIN 6
#define PRECIOHAMBURGUESA 50
#define PRECIOSALCHICHA 35


typedef struct orden
{
    int ID_Producto;
    int hamburguesa;
    int salchicha;
    struct orden *siguiente;
}Pedido;


int login();
void abreCaja(float *dinero, int *stockHam, int *stockSal);
Pedido *crearNodo();
int contarPreparacion(Pedido **p);
void agregarPedido(Pedido **p, float *dinero, int *stockHam, int *stockSal, int *id);
void verPedidos(Pedido **p);
void sacarPedido(Pedido **p, int *pedidosSacados, int *salchichasVendidas, int *hamburguesasVendidas);
void cierreCaja(int pedidosSacados, float dineroInicial, float dineroCaja, Pedido **p);
void liberar(Pedido **p);
int imprimir_reporte();


int flagEntrega=0; // para probar visualizacion del reporte


int main()
{
    int opcion=0;
    Pedido *p=NULL;
    int pedidosEntregados=0;
    float dineroCaja=0;
    int stockHamburguesa=0;
    int stockSalchicha=0;
    int salchichasVendidas=0;
    int hamburguesasVendidas=0;
    int id=101;
    float dineroInicial=0;

    if (login())
    {
        abreCaja(&dineroCaja,&stockHamburguesa,&stockSalchicha);
        dineroInicial=dineroCaja;

        while(1)
        {
            system("cls");
            printf("Pedidos en preparacion (-1 = vacio): %i\n", contarPreparacion(&p));
            printf("\nMENU DE OPCIONES \n\n");
            printf("\t1. Agregar pedido. \n");
            printf("\t2. Ver pedidos. \n");
            printf("\t3. Entregar pedido. \n");
            printf("\t4. Reporte diario. \n");
            printf("\t5. Cierre de caja. \n");
            scanf("%i", &opcion);

            switch(opcion)
            {
                case 1: agregarPedido(&p, &dineroCaja, &stockHamburguesa, &stockSalchicha, &id);
                        break;
                case 2: verPedidos(&p);
                        break;
                case 3: sacarPedido(&p,&pedidosEntregados,&salchichasVendidas,&hamburguesasVendidas);
                        break;
                case 4: imprimir_reporte();
                        break;
                case 5: cierreCaja(pedidosEntregados, dineroInicial, dineroCaja, &p);
                        exit(0);
                        break;
                default: printf("No se eligio una opcion valida\n");
            }
        }
    }

    else
    {
        printf("\n\tHa sobrepasado el numero maximo de intentos permitidos.\n");
    }

    return 0;
}


int login()
{
    char usuario[LONGITUDLOGIN+1];
    char clave[LONGITUDLOGIN+1];
    int intento=0;
    int ingresa=0;
    char caracter;
    int ilogin=0;

    do
    {
        system("cls");
        printf("\n\t\t\tINICIO DE SESION\n");
        printf("\t\t\t---------------\n");
        printf("\n\tUSUARIO: ");
        gets(usuario);
        printf("\tCLAVE: ");

        while(caracter=getch())
        {
            if(caracter==13) // 13 es el enter
            {
                clave[ilogin]='\0';
                break;

            }
            else if(caracter==8) // backspace es el 8
            {
                if (ilogin>0)
                {
                    ilogin--;
                    printf("\b \b");
                }

            }
            else
            {
                if (ilogin<LONGITUDLOGIN)
                {
                    printf("*");
                    clave[ilogin]=caracter;
                    ilogin++;
                }
            }
        }

        if(strcmp(usuario,USUARIO)==0&&strcmp(clave,CLAVE)==0)
        {
            ingresa=1;

        }
        else
        {
            intento++;
            printf("\n\t\n\tUsuario y/o clave son incorrectos.\n\tIntento %i de 3.",intento);
            getchar();
        }

    } while(intento<3&&ingresa==0);

    return ingresa;
}


void abreCaja(float *dinero, int *stockHam, int *stockSal)
{
    printf("\n\n");
    printf("\tDinero inicial en caja: $ ");
    scanf("%f",dinero);
    printf("\tStock hamburguesa: ");
    scanf("%i",stockHam);
    printf("\tStock salchichas: ");
    scanf("%i",stockSal);
}


Pedido *crearNodo()
{
    Pedido * nuevo = malloc(sizeof(Pedido));
    nuevo->siguiente = NULL;
    nuevo->hamburguesa=0;
    nuevo->salchicha=0;

    return nuevo;
}


int contarPreparacion(Pedido **p)
{
    int cant=0;

    if(*p==NULL)
    {
        return -1;
    }

    Pedido *recorrer=*p;

    while(recorrer!=NULL)
    {
        cant++;
        recorrer=recorrer->siguiente;
    }

    return cant;
}


void agregarPedido(Pedido **p, float *dinero, int *stockHam, int *stockSal, int *id)
{
    float auxDinero = 0;
	int metodoPago = 0;

    Pedido *nuevo = crearNodo();

    if(nuevo==NULL)
    {
        printf("\nNo se puede agregar un pedido.\n");
        return;
    }

    

    printf("\nNUEVO PEDIDO \n\n");

    printf("Cantidad de Salchichas: ");
    scanf("%i", &nuevo->salchicha);
    if(nuevo->salchicha>*stockSal)
    {
        printf("Solo quedan %i salchichas. \n",*stockSal);
        free(nuevo);
        system("pause");
        return;
    }

    printf("Cantidad de Hamburguesas: ");
    scanf("%i", &nuevo->hamburguesa);
    if(nuevo->hamburguesa>*stockHam)
    {
        printf("Solo quedan %i hamburguesas. \n",*stockHam);
        free(nuevo);
        system("pause");
        return;
    }
    *stockSal-=nuevo->salchicha;
    auxDinero+=nuevo->salchicha*PRECIOSALCHICHA;
    *stockHam-=nuevo->hamburguesa;
    auxDinero+=nuevo->hamburguesa*PRECIOHAMBURGUESA;
    *dinero+=auxDinero;

    nuevo->ID_Producto = *id;
    (*id)++;

    printf("\n\t%i Salchichas $ %i \n\t%i Hamburguesas $ %i \n\n\tTotal: $ %0.2f \n\n",nuevo->salchicha,PRECIOSALCHICHA,nuevo->hamburguesa,PRECIOHAMBURGUESA,auxDinero);

	printf("Elija un metodo de pago:\n\t1-Efectivo\n\t2-Tarjeta de credito\n\n");
	scanf("%i", &metodoPago);

    if(*p==NULL)
    {
        *p=nuevo;
    }
    else
    {
        Pedido *ultimo=*p;

        while(ultimo->siguiente!=NULL)
        {
            ultimo = ultimo->siguiente;
        }
        ultimo->siguiente = nuevo;
    }

    system("pause");
}


void verPedidos(Pedido **p)
{
    if(*p==NULL)
    {
        printf("\nNo hay pedidos en curso.\n");
        system("pause");
        return;
    }

    Pedido *recorrer = *p;

    printf("\nPEDIDOS \n");
    while(recorrer!=NULL)
    {
        printf("Pedido: %i \n",recorrer->ID_Producto);
        printf("\tPancho: %i \n",recorrer->salchicha);
        printf("\tHamburguesa: %i \n",recorrer->hamburguesa);
        recorrer = recorrer->siguiente;
        printf("\n");
    }

    system("pause");
}


void sacarPedido(Pedido **p, int *pedidosSacados, int *salchichasVendidas, int *hamburguesasVendidas)
{
    if(*p==NULL)
    {
        printf("\nNo hay pedidos por entregar. \n");
        system("pause");
        return;
    }

    Pedido *borrar=*p;
    *p=borrar->siguiente;
    (*salchichasVendidas)+=borrar->salchicha;
    (*hamburguesasVendidas)+=borrar->hamburguesa;

    free(borrar);

    printf("\nPedido entregado. \n");
    (*pedidosSacados)++;

    FILE *archivo=fopen("reporte.bin", "wb");
    if(archivo==NULL)
    {
        printf("Error al abrir el archivo.");
        system("pause");
        return;
    }

    fwrite(pedidosSacados,sizeof(int),1,archivo);
    fwrite(salchichasVendidas,sizeof(int),1,archivo);
    fwrite(hamburguesasVendidas,sizeof(int),1,archivo);
    fclose(archivo);

    flagEntrega=1;

    system("pause");
}


void cierreCaja(int pedidosSacados, float dineroInicial, float dineroCaja, Pedido **p)
{
    float dineroFacturado=dineroCaja-dineroInicial;

    FILE *archivo=fopen("log.txt", "wt");
    if(archivo==NULL)
    {
        printf("Error al abrir el archivo.");
        return;
    }

    fprintf(archivo,"Productos vendidos: %i\n",pedidosSacados);
    fprintf(archivo, "Dinero facturado: %.2f", dineroFacturado);

    fclose(archivo);

    liberar(p);

    printf("\nCierre de caja exitoso. \n");
}


void liberar(Pedido **p)
{
    Pedido *recorrer=*p;
    Pedido *borrar;

    while(recorrer!=NULL)
    {
        borrar=recorrer->siguiente;
        free(recorrer);
        recorrer=borrar;
    }
}


int imprimir_reporte()
{
    int a[3], i=0;

    if(flagEntrega!=1)
    {
        printf("\nNO HAY PEDIDOS ENTREGADOS \n\n");
        system("pause");
        return;
    }

    FILE *archivo=fopen("reporte.bin", "rb");
    if(archivo==NULL)
    {
        printf("Error al abrir el archivo.");
        system("pause");
        return;
    }

    do
    {
       fread (&a[i],sizeof(int),1,archivo);
       i++;
    }while(!feof(archivo));

    printf("\nPedidos sacados: %i", a[0]);
    printf("\nSalchichas vendidas: %i", a[1]);
    printf("\nHamburguesas vendidas: %i", a[2]);
    printf("\n\n");

    system("PAUSE");

    fclose(archivo);

    return 0;
}
